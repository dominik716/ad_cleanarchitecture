﻿using Microsoft.AspNetCore.Identity;

namespace AD_CleanArchitecture.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
