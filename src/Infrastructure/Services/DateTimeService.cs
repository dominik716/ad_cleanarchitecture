﻿using AD_CleanArchitecture.Application.Common.Interfaces;
using System;

namespace AD_CleanArchitecture.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
