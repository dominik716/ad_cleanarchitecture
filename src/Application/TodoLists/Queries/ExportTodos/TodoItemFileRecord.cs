﻿using AD_CleanArchitecture.Application.Common.Mappings;
using AD_CleanArchitecture.Domain.Entities;

namespace AD_CleanArchitecture.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
