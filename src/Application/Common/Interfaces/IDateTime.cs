﻿using System;

namespace AD_CleanArchitecture.Application.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
