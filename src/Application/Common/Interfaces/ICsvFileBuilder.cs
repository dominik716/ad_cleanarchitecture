﻿using AD_CleanArchitecture.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace AD_CleanArchitecture.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
